to start project use 'docker-compose up' command (It may take some time cause of downloading maven dependencies).It starts AppliftingTask, SampleEndpoints modules and mysql db.

http://localhost:8080/api/admin/* to use AppliftingTask module services.
http://localhost:8081/api/sample/* to see sample endpoints for monitoring. 
 
We have two usesr inserted into databse: 'Applifting' and 'Batman'

while using api/admin services send one of these tokens in the header ( header name : "authentication") . Auth token for users :

Applifting -> Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJBcHBsaWZ0aW5nIiwiZXhwIjoxMDE2NTA4NDczNzYsImlhdCI6MTY1MDg0NzM3NiwianRpIjoiYWYxNjgxOTktMDIzZi00ZmExLWJlZDMtMTIwMzdmN2MyMWE2In0.sv21NX8FwylfbORkeeU4phLqjwZYebopE2AfpChJ9n0B2kOy4PpR3-w5ekiVo5GTDZHLPj1MsJr7-clma0M04Q. 

Batman -> Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJCYXRtYW4iLCJleHAiOjEwMTY1MDg0NzM3NiwiaWF0IjoxNjUwODQ3Mzc2LCJqdGkiOiJiNzU2NWM0Mi1kNjkwLTQwZGUtODA3Ny1kNWI2ZjNiNjVjMjAifQ.3elW1aJ-IJPFLoRlkEqpZ5xMRSqDnoOQU9S2QFZKMZiQsHQrLKHPM116Fc7jsZ8LwQV-IogsWzaMKbIGknB_qA. 


SERVICES :

PUT , http://localhost:8080/api/admin/endpoint ,
example request body { "name" : "endpoint 2 monitoring" , "url" : "/api/sample/endpoint2" ,"monitoredInterval" : "1200"}
description :  creates or updates endpoint

DELETE , http://localhost:8080/api/admin/endpoint/{id}
description :  deletes endpoint

GET ,  http://localhost:8080/api/admin/endpoint
description : shows endpoints by user

GET ,  http://localhost:8080/api/admin/endpoint{id} 
description : shows last 10 result of particular endpoint details 




Sample endpoints to create for users :  /api/sample/endpoint2 , /api/sample/endpoint3
