package com.SampleEndpoints.controller;

import com.SampleEndpoints.model.SampleModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.nio.charset.Charset;
import java.util.Random;

@RestController
@RequestMapping("/sample")
public class SampleEndpointsControler {

    @GetMapping("/endpoint1")
    public ResponseEntity<SampleModel> createSampleMode1(){
        return new ResponseEntity<>( getSampleModel(),HttpStatus.OK);
    }



    @GetMapping("/endpoint2")
    public ResponseEntity<SampleModel> createSampleMode2(){
        return new ResponseEntity<>(getSampleModel(),HttpStatus.OK);
    }

    @GetMapping("/endpoint3")
    public ResponseEntity<SampleModel> createSampleMode3(){
        return new ResponseEntity<>( getSampleModel(),HttpStatus.BAD_REQUEST );
    }


    private SampleModel getSampleModel(){
        byte[] array = new byte[5] ;
        new Random().nextBytes(array);
        String randomString = new String(array, Charset.forName("UTF-8") );
        return new SampleModel(randomString,randomString,randomString);
    }
}
