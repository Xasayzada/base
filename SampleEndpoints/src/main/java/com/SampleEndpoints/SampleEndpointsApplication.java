package com.SampleEndpoints;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleEndpointsApplication {
    public static void main(String[] args) {
        SpringApplication.run(SampleEndpointsApplication.class, args);
    }

}
