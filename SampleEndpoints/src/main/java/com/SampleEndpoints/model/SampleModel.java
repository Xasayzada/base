package com.SampleEndpoints.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class SampleModel {
    private String var1;
    private String var2;
    private String var3;
}
