package com.AppliftingTask.util;

import com.AppliftingTask.model.MonitoredEndpoint;
import com.AppliftingTask.model.MonitoringResult;
import com.AppliftingTask.repo.MonitoringRepo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;
import java.util.UUID;

@Component
@RequiredArgsConstructor
@Slf4j
public class EndpointCaller {

    @Value("${sampleendpointhost}")
    private String sampleendpointhost;
    private final MonitoringRepo monitoringRepo;
    private final ObjectMapper objectMapper;
    private RestTemplate restTemplate = new RestTemplate();

    @Async("threadPoolTaskExecutor")
    @Scheduled(fixedDelay = 10000)
    public void scheduleFixedDelayTask() throws JsonProcessingException {
       List<MonitoredEndpoint> endpoints =  monitoringRepo.getMonitoredEndpoints(null);

        for (MonitoredEndpoint endpoint : endpoints) {
            log.info(sampleendpointhost + endpoint.getUrl() + " URL is monitoring");
            ResponseEntity<Object> response = null;
            try {
                response = restTemplate.exchange(URI.create( sampleendpointhost + endpoint.getUrl()), HttpMethod.GET,null, Object.class);
            }catch (HttpClientErrorException e1){
                response =  new ResponseEntity<>(e1.getMessage() , e1.getStatusCode());
            }catch (Exception e){
                response =  new ResponseEntity<>(e.getMessage() ,HttpStatus.INTERNAL_SERVER_ERROR);
            }

            MonitoringResult monitoringResult = MonitoringResult.builder()
                    .id(UUID.randomUUID().toString())
                    .endpointId(endpoint.getId())
                    .payload(objectMapper.writeValueAsString(response.getBody()))
                    .statusCode(response.getStatusCode().value()).build();

            log.info("monitoringResult is : " +monitoringResult);

            monitoringRepo.createResult(monitoringResult);
        }
    }
}
