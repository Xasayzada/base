package com.AppliftingTask.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class MonitoringResult {

    private String id;
    private LocalDateTime checkedDate;
    private Integer statusCode;
    private String payload;
    private String endpointId ;
}
