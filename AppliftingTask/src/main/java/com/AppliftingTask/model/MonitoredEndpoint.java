package com.AppliftingTask.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@NoArgsConstructor
@Data
public class MonitoredEndpoint {
    private String id;
    private String name;
    @NotNull
    private String url;
    private LocalDateTime createdDate;
    private String lastCheckedDate;
    private int monitoredInterval;
}
