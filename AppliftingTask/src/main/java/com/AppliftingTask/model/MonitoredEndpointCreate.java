package com.AppliftingTask.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class MonitoredEndpointCreate {

    private String id;
    private String name;
    @NotNull
    private String url;
    @Min(1)
    private int monitoredInterval;
    @JsonIgnore
    private User user;
}
