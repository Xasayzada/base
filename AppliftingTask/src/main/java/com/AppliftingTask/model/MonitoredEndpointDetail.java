package com.AppliftingTask.model;


import lombok.Data;

import java.util.List;

@Data
public class MonitoredEndpointDetail extends  MonitoredEndpoint{

    private List<MonitoringResult> resultList;
}
