package com.AppliftingTask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class AppliftingTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppliftingTaskApplication.class, args);
	}

}
