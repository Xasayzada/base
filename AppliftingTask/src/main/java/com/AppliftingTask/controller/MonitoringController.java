package com.AppliftingTask.controller;


import com.AppliftingTask.model.MonitoredEndpoint;
import com.AppliftingTask.model.MonitoredEndpointCreate;
import com.AppliftingTask.model.MonitoredEndpointDetail;
import com.AppliftingTask.service.MonitoringService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin")
@RequiredArgsConstructor
public class MonitoringController {

    private final MonitoringService monitoringService;


    @PutMapping("/endpoint")
    public ResponseEntity<MonitoredEndpointCreate> createOrUpdateEndpoint(@RequestBody @Validated MonitoredEndpointCreate monitoredEndpoint){
        return new ResponseEntity<>(monitoringService.createOrUpdateEndpoint(monitoredEndpoint),HttpStatus.CREATED);
    }


    @DeleteMapping("/endpoint/{id}")
    public ResponseEntity deleteEndpoint (@PathVariable String id){
        int deletedRows = monitoringService.deleteEndpoint(id);
        if(deletedRows == 0){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(HttpStatus.OK);
    }


    @GetMapping("/endpoint")
    public List<MonitoredEndpoint> getEndpoints (){
        return monitoringService.getMonitoredEndpoints();
    }

    @GetMapping("/endpoint/{endpointId}")
    public MonitoredEndpointDetail getEndpointDetail (@PathVariable String endpointId){
        return monitoringService.getMonitoredEndpointDetail(endpointId);
    }

}
