package com.AppliftingTask.service;

import com.AppliftingTask.model.MonitoredEndpoint;
import com.AppliftingTask.model.MonitoredEndpointCreate;
import com.AppliftingTask.model.MonitoredEndpointDetail;

import java.util.List;

public interface MonitoringService {

    MonitoredEndpointCreate createOrUpdateEndpoint(MonitoredEndpointCreate monitoredEndpoint);

    int deleteEndpoint(String id);

    List<MonitoredEndpoint> getMonitoredEndpoints();

    MonitoredEndpointDetail getMonitoredEndpointDetail(String endpointId);
}
