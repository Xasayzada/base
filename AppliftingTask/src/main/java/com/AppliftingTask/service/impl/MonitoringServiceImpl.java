package com.AppliftingTask.service.impl;

import com.AppliftingTask.config.security.JwtService;
import com.AppliftingTask.model.MonitoredEndpoint;
import com.AppliftingTask.model.MonitoredEndpointCreate;
import com.AppliftingTask.model.MonitoredEndpointDetail;
import com.AppliftingTask.model.User;
import com.AppliftingTask.repo.MonitoringRepo;
import com.AppliftingTask.service.MonitoringService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.PrimitiveIterator;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class MonitoringServiceImpl implements MonitoringService {

    private final MonitoringRepo monitoringrepo;
    private final JwtService jwtService;

    @Override
    public MonitoredEndpointCreate createOrUpdateEndpoint(MonitoredEndpointCreate monitoredEndpoint) {
        monitoredEndpoint.setUser(User.builder().id(jwtService.getUserIdFromToken()).build());

        if (Objects.isNull(monitoredEndpoint.getId())) {
            monitoredEndpoint.setId(UUID.randomUUID().toString());
            monitoringrepo.createEndpoint(monitoredEndpoint);
            return monitoredEndpoint;
        }

        monitoringrepo.updateEndpoint(monitoredEndpoint);
        return monitoredEndpoint;
    }


    @Override
    public int deleteEndpoint(String id) {
        return monitoringrepo.deleteEndpoint(id, jwtService.getUserIdFromToken());
    }


    @Override
    public List<MonitoredEndpoint> getMonitoredEndpoints() {

        return monitoringrepo.getMonitoredEndpoints(jwtService.getUserIdFromToken());
    }

    @Override
    public MonitoredEndpointDetail getMonitoredEndpointDetail(String endpointId) {
        return monitoringrepo.getMonitoredEndpointDetail(endpointId, jwtService.getUserIdFromToken());
    }
}
