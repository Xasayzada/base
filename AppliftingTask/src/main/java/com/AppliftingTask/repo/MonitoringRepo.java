package com.AppliftingTask.repo;

import com.AppliftingTask.model.*;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Optional;

@Mapper
public interface MonitoringRepo {
    void createEndpoint(MonitoredEndpointCreate monitoredEndpoint);

    void updateEndpoint(MonitoredEndpointCreate monitoredEndpoint);

    int deleteEndpoint(String id, String userId);

    List<MonitoredEndpoint> getMonitoredEndpoints(String userId);

    void  createResult(MonitoringResult result);

    MonitoredEndpointDetail getMonitoredEndpointDetail(String endpointId, String userId);



}
