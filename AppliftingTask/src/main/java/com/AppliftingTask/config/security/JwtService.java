package com.AppliftingTask.config.security;


import com.AppliftingTask.util.HttpUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class JwtService {

    private final HttpUtil httpUtil;
    private static String secretKEy = "secretKey";

    public String getUsernameFromToken() {
        return getClaimFromToken(httpUtil.getTokenFromHeader(), Claims::getSubject);
    }

    public String getUserIdFromToken() {
        return getClaimFromToken(httpUtil.getTokenFromHeader(), Claims::getId);
    }


    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        Claims claims =Jwts.parser().setSigningKey(secretKEy).parseClaimsJws(token).getBody();
        return claimsResolver.apply(claims);
    }

    public boolean validateToken(String token) {
        try {
            if (!isTokenExpired(token)) {
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }

    public boolean isTokenExpired(String token) {
        return getClaimFromToken(token, Claims::getExpiration).before(new Date(System.currentTimeMillis()));
    }


}
