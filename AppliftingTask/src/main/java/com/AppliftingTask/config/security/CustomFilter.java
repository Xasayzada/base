package com.AppliftingTask.config.security;

import com.AppliftingTask.model.User;
import com.AppliftingTask.repo.MonitoringRepo;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@Service
@RequiredArgsConstructor
public class CustomFilter extends OncePerRequestFilter {
    private final JwtService jwtService;


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        String accessToken = request.getHeader("authentication");

        if (Objects.isNull(accessToken) || accessToken.isEmpty()) {
            response.setStatus(400);
        } else {
            if (Objects.nonNull(accessToken) && accessToken.startsWith("Bearer ") && jwtService.validateToken(accessToken.substring(7))) {
                filterChain.doFilter(request, response);
            } else {
                response.setStatus(401);
            }
        }


    }


}
