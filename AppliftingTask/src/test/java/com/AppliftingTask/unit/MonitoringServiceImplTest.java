package com.AppliftingTask.unit;

import com.AppliftingTask.config.security.JwtService;
import com.AppliftingTask.model.MonitoredEndpointCreate;
import com.AppliftingTask.model.User;
import com.AppliftingTask.repo.MonitoringRepo;
import com.AppliftingTask.service.impl.MonitoringServiceImpl;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class MonitoringServiceImplTest {

    @Mock
    private MonitoringRepo monitoringRepo;
    @Mock
    private JwtService jwtService;
    @InjectMocks
    private MonitoringServiceImpl monitoringService  ;


    private static MonitoredEndpointCreate monitoredEndpointCreate;



    @BeforeAll
    static void setUp(){
        monitoredEndpointCreate = new MonitoredEndpointCreate(null,"test", "test", 1000, new User());
    }

    @Test
    @Order(1)
    public void createOrUpdateEndpointTestCreateOp(){

        monitoringService.createOrUpdateEndpoint(monitoredEndpointCreate);

        verify(jwtService, times(1)).getUserIdFromToken();
        verify(monitoringRepo, times(1)).createEndpoint(monitoredEndpointCreate);

    }

    @Test
    @Order(2)
    public void createOrUpdateEndpointTestUpdateOp(){
        monitoredEndpointCreate.setId("test");
        monitoringService.createOrUpdateEndpoint(monitoredEndpointCreate);

        verify(jwtService, times(1)).getUserIdFromToken();
        verify(monitoringRepo, times(1)).updateEndpoint(monitoredEndpointCreate);

    }
}
